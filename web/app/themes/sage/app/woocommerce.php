<?php

/**
 * Hook removals
 */
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

/**
 * Hook additions
 */
add_action('bedepo_add_to_cart', 'woocommerce_template_single_add_to_cart', 10);
add_action('bedepo_related', 'woocommerce_output_related_products', 10);
