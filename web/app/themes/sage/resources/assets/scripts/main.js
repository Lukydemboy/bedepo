// import external dependencies
import 'jquery';

import Vue from 'vue'
import SlideUpDown from 'vue-slide-up-down'

Vue.component('slide-up-down', SlideUpDown)
Vue.component('faq', require('./vue/FAQ.vue').default)
Vue.component('loader', require('./vue/Loader.vue').default)
Vue.component('product-filter', require('./vue/ProductFilter.vue').default)
Vue.component('contact-form', require('./vue/forms/ContactForm.vue').default)
Vue.component('product-catalog', require('./vue/ProductCatalog.vue').default)
Vue.component('product-preview', require('./vue/ProductPreview.vue').default)
Vue.component('product-filters', require('./vue/ProductFilters.vue').default)
Vue.component('form-feedback', require('./vue/forms/FormFeedback.vue').default)
Vue.component('product-inquery', require('./vue/forms/ProductInquery.vue').default)
Vue.component('product-ordering', require('./vue/dropdowns/ProductOrdering.vue').default)
Vue.component('product-lightbox', require('./vue/lightboxes/ProductLightbox.vue').default)
Vue.component('gallery-lightbox', require('./vue/lightboxes/GalleryLightbox.vue').default)
Vue.component('product-gallery-lightbox', require('./vue/lightboxes/ProductGalleryLightbox.vue').default)

// FontAwesome
import { library, dom } from '@fortawesome/fontawesome-svg-core'

import {
	faFacebookF,
	faTwitter,
	faLinkedinIn,
	faInstagram
} from '@fortawesome/free-brands-svg-icons'

library.add(
	faFacebookF,
	faTwitter,
	faLinkedinIn,
	faInstagram
)

dom.watch()

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

new Vue({
	el: '#app',
})

import toggleNav from './toggle-mobile-menu'
toggleNav.init()

// Load Events
jQuery(document).ready(() => routes.loadEvents());
