export default {
	init() {
		this.registerEventListeners()
	},

	registerEventListeners() {
		const navToggler = document.getElementById('navToggler');
		const navClose = document.getElementById('navClose');
		const mobileNavigation = document.getElementById('mobileNav');

		if (!navToggler || !navClose || !mobileNavigation) return;

		navToggler.addEventListener('click', () => {
			if (!mobileNavigation) return

			mobileNavigation.classList.add('active');
		})

		navClose.addEventListener('click', () => {
			if (!mobileNavigation) return

			mobileNavigation.classList.remove('active');
		})
	},

}
