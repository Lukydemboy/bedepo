{{--

My Account Dashboard

Shows the first intro screen on the account dashboard.

This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.

HOWEVER, on occasion WooCommerce will need to update template files and you
(the theme developer) will need to copy the new files to your theme to
maintain compatibility. We try to do this as little as possible, but it does
happen. When this occurs the version of the template file will be bumped and
the readme will list any important changes.

@see     https://docs.woocommerce.com/document/template-structure/
@package WooCommerce\Templates
@version 4.4.0

 --}}

@php
if (!defined('ABSPATH')) {
exit; // Exit if accessed directly.
}

$allowed_html = array(
'a' => array(
'href' => array(),
),
);
@endphp

<h2 class="text-2xl mt-0 mb-sm">
	{{ sprintf(__('Welcome %s!', 'sage'), esc_html($current_user->display_name)) }}
</h2>

<div class="row mb--xs">
	<div class="col-lg-6 mb-xs">
		<!-- Dashboard Action -->
		<div class="dashboard-action">
			<div class="dashboard-action__icon">
				<i class="icon-order"></i>
			</div>
			<div class="dashboard-action__description">
				<h4 class="text-base mt-0">
					{{ __('My orders', 'sage') }}
				</h4>
				<p>
					{{ __('View your recent orders and track their status.', 'sage') }}
				</p>
			</div>
			<a href="{{ wc_get_endpoint_url('orders') }}" class="dashboard-action__link"></a>
		</div>
		<!-- End Dashboard Action -->
	</div>
	<div class="col-lg-6 mb-xs">
		<!-- Dashboard Action -->
		<div class="dashboard-action">
			<div class="dashboard-action__icon">
				<i class="icon-addresses"></i>
			</div>
			<div class="dashboard-action__description">
				<h4 class="text-base mt-0">
					{{ __('My addresses', 'sage') }}
				</h4>
				<p>
					{{ __('Edit the addresses that are used during checkout by default.', 'sage') }}
				</p>
			</div>
			<a href="{{ wc_get_endpoint_url('edit-account') }}" class="dashboard-action__link"></a>
		</div>
		<!-- End Dashboard Action -->
	</div>
	<div class="col-lg-6 mb-xs">
		<!-- Dashboard Action -->
		<div class="dashboard-action">
			<div class="dashboard-action__icon">
				<i class="icon-id"></i>
			</div>
			<div class="dashboard-action__description">
				<h4 class="text-base mt-0">
					{{ __('Account details', 'sage') }}
				</h4>
				<p>
					{{ __('Edit your personal account info and details.', 'sage') }}
				</p>
			</div>
			<a href="{{ wc_get_endpoint_url('edit-address') }}" class="dashboard-action__link"></a>
		</div>
		<!-- End Dashboard Action -->
	</div>
	<div class="col-lg-6 mb-xs">
		<!-- Dashboard Action -->
		<div class="dashboard-action">
			<div class="dashboard-action__icon">
				<i class="icon-sign-out"></i>
			</div>
			<div class="dashboard-action__description">
				<h4 class="text-base mt-0">
					{{ __('Sign out', 'sage') }}
				</h4>
				<p>
					{{ __('Thanks for stopping by, see you next time!.', 'sage') }}
				</p>
			</div>
			<a href="{{ wp_logout_url(get_permalink(wc_get_page_id('myaccount'))) }}"
				class="dashboard-action__link"></a>
		</div>
		<!-- End Dashboard Action -->
	</div>
</div>

@php
/**
* My Account dashboard.
*
* @since 2.6.0
*/
do_action('woocommerce_account_dashboard');

/**
* Deprecated woocommerce_before_my_account action.
*
* @deprecated 2.6.0
*/
do_action('woocommerce_before_my_account');

/**
* Deprecated woocommerce_after_my_account action.
*
* @deprecated 2.6.0
*/
do_action('woocommerce_after_my_account');
@endphp
