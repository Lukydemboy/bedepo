{{--

Login Form

This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.

HOWEVER, on occasion WooCommerce will need to update template files and you
(the theme developer) will need to copy the new files to your theme to
maintain compatibility. We try to do this as little as possible, but it does
happen. When this occurs the version of the template file will be bumped and
the readme will list any important changes.

@see     https://docs.woocommerce.com/document/template-structure/
@package WooCommerce\Templates
@version 4.1.0

--}}

@php
if (! defined('ABSPATH')) {
exit; // Exit if accessed directly.
}

do_action('woocommerce_before_customer_login_form');
@endphp

@if ('yes' === get_option('woocommerce_enable_myaccount_registration'))

<div class="row justify-content-between mb--xs" id="customer_login">

	<div class="col-xl-4 col-lg-5 mb-xs">

		@endif

		<div class="card shadow--lg">
			<div class="card__body card__body--padded-lg">

				<h2 class="mt-0">
					{!! esc_html_e('Login', 'woocommerce') !!}
				</h2>

				<form method="post">


					@php
					do_action('woocommerce_login_form_start');
					@endphp

					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="username">{!! esc_html_e('Username or email address', 'woocommerce') !!}&nbsp;<span
								class="required">*</span></label>
						<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username"
							id="username" autocomplete="username"
							value="{!! (! empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : '' !!}" />
					</p>
					<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
						<label for="password">{!! esc_html_e('Password', 'woocommerce') !!}&nbsp;<span
								class="required">*</span></label>
						<input class="woocommerce-Input woocommerce-Input--text input-text" type="password"
							name="password" id="password" autocomplete="current-password" />
					</p>

					@php
					do_action('woocommerce_login_form');
					@endphp

					<p class="form-row">
						@php
						wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce');
						@endphp
						<button type="submit" class="woocommerce-button button woocommerce-form-login__submit"
							name="login" value="{!! esc_attr_e('Log in', 'woocommerce') !!}">
							{!! esc_html_e('Log in', 'woocommerce') !!}
						</button>
					</p>
					<p class="woocommerce-LostPassword lost_password mb-0 mt-xs">
						<a href="{!! esc_url(wp_lostpassword_url()) !!}" class="text-body">
							{!! esc_html_e('Lost your password?', 'woocommerce') !!}
						</a>
					</p>

					@php
					do_action('woocommerce_login_form_end');
					@endphp
				</form>
			</div>
		</div>

		@if ('yes' === get_option('woocommerce_enable_myaccount_registration'))

	</div>

	<div class="col-lg-7 mb-xs">

		<h2>
			{!! esc_html_e('Register', 'woocommerce') !!}
		</h2>

		<form method="post">

			@php
			do_action('woocommerce_register_form_tag');
			do_action('woocommerce_register_form_start');
			@endphp

			@if ('no' === get_option('woocommerce_registration_generate_username'))

			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="reg_username">{!! esc_html_e('Username', 'woocommerce') !!}&nbsp;<span
						class="required">*</span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username"
					id="reg_username" autocomplete="username"
					value="{!! (! empty($_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : '' !!}" />
			</p>

			@endif

			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="reg_email">{!! esc_html_e('Email address', 'woocommerce') !!}&nbsp;<span
						class="required">*</span></label>
				<input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email"
					id="reg_email" autocomplete="email"
					value="{!! (! empty($_POST['email'])) ? esc_attr(wp_unslash($_POST['email'])) : '' !!}" />
			</p>

			@if ('no' === get_option('woocommerce_registration_generate_password'))

			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="reg_password">{!! esc_html_e('Password', 'woocommerce') !!}&nbsp;<span
						class="required">*</span></label>
				<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password"
					id="reg_password" autocomplete="new-password" />
			</p>

			@else

			<p>
				{!! esc_html_e('A password will be sent to your email address.', 'woocommerce') !!}
			</p>

			@endif

			@php
			do_action('woocommerce_register_form');
			@endphp

			<p class="woocommerce-form-row form-row">
				@php
				wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce');
				@endphp
				<button type="submit"
					class="woocommerce-Button woocommerce-button button woocommerce-form-register__submit"
					name="register" value="{!! esc_attr_e('Register', 'woocommerce') !!}">
					{!! esc_html_e('Register', 'woocommerce') !!}
				</button>
			</p>

			@php
			do_action('woocommerce_register_form_end');
			@endphp

		</form>

	</div>

</div>
@endif

@php
do_action('woocommerce_after_customer_login_form');
@endphp
