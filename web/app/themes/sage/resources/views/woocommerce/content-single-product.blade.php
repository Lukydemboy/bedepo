{{--

The template for displaying product content in the single-product.php template

This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.

HOWEVER, on occasion WooCommerce will need to update template files and you
(the theme developer) will need to copy the new files to your theme to
maintain compatibility. We try to do this as little as possible, but it does
happen. When this occurs the version of the template file will be bumped and
the readme will list any important changes.

@see     https://docs.woocommerce.com/document/template-structure/
@package WooCommerce\Templates
@version 3.6.0

 --}}

@php
defined( 'ABSPATH' ) || exit;

global $product;

/**
* Hook: woocommerce_before_single_product.
*
* @hooked woocommerce_output_all_notices - 10
*/
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
@endphp
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?> class="single-product">
	<div class="gray-banner"></div>
	<div class="container">
		<div class="d-flex pt-xl">
			<h1 class="title mb-0">{!! $product->get_title() !!}</h1>
		</div>
		<div class="row justify-content-between pt-sm pb-xl t-flex-direction-column-reverse">
			<div class="col-lg-6">
				<div class="text pb-sm">
					{!! $product->get_description() !!}
				</div>

				{!! wc_display_product_attributes($product) !!}

				<div class="d-flex justify-content-between align-items-center">
					<div class="product-price-label mt-xs mb-xs">
						{!! $product->get_price_html() !!}
					</div>

					<!-- Product Inquery -->
					<product-inquery animal="{{ $product->get_title() }}" animal-url="{{ get_permalink($product->get_id()) }}"></product-inquery>
					<!-- End Product Inquery -->

				</div>
			</div>
			<div class="col-lg-5">
				<div class="d-flex t-flex-direction-column">
					@php
						$gallery_images = $product->get_gallery_image_ids();

						$all_images = array_map(function($image) {
							return wp_get_attachment_image_src($image, 'full')[0];
						}, $gallery_images);

						array_unshift($all_images, get_the_post_thumbnail_url($product->get_ID(), 'full'));
					@endphp

					@if (has_post_thumbnail($product->get_ID()))
						<div class="single-product__image">
							<product-lightbox :sources="{{ json_encode($all_images) }}" :index="0" preview-image="{{ get_the_post_thumbnail_url($product->get_ID(), 'full') }}"></product-lightbox>
						</div>
					@endif

					<!-- Product Gallery -->
					@if(!empty($gallery_images))
						<div class="product-gallery d-flex flex-column t-flex-direction-row t-justify-content-space-between md-pl-0">
							@for ($i = 1; $i < count($all_images); $i++)
								<div class="product-gallery__image product-gallery__image--small">
									<product-gallery-lightbox :sources="{{ json_encode($all_images) }}" :index="{{ $i }}" preview-image="{{ $all_images[$i] }}"></product-lightbox>

									{{-- <img class="single-product__image--gallery" src="{{ $all_images[$i] }}" alt=""> --}}
								</div>
							@endfor
							{{-- @foreach ($gallery_images as $img)
								<div class="product-gallery__image">
									<img class="single-product__image--gallery" src="{{ wp_get_attachment_image_src($img, 'thumbnail')[0] }}" alt="">
								</div>
							@endforeach --}}
						</div>
					@endif
					<!-- End Product Gallery -->

				</div>
			</div>
		</div>

		@if (get_field('sire', $product->get_id()) || get_field('dam', $product->get_id()) || get_field('offspring', $product->get_id()))
			<hr class="pb-lg">
		@endif

		<!-- Product Parents -->
		@php
			$sire = get_field('sire', $product->get_id());
			$dam = get_field('dam', $product->get_id());
		@endphp

		@if ($sire || $dam)
			@include('partials.parents', [
				'sire' => $sire,
				'dam' => $dam,
			])
		@endif
		<!-- End Product Parents -->

		<!-- Offspring -->
		@if (get_field('offspring', $product->get_id()))
			@include('partials.offspring', [
				'offspring' => get_field('offspring', $product->get_id()),
			])
		@endif
		<!-- End Offspring -->

	</div>
</div>

@php
do_action('woocommerce_after_single_product');
@endphp
