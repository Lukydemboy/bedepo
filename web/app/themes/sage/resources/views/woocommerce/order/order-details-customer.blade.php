{{--

Order Customer Details

This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-customer.php.

HOWEVER, on occasion WooCommerce will need to update template files and you
(the theme developer) will need to copy the new files to your theme to
maintain compatibility. We try to do this as little as possible, but it does
happen. When this occurs the version of the template file will be bumped and
the readme will list any important changes.

@see     https://docs.woocommerce.com/document/template-structure/
@package WooCommerce\Templates
@version 3.4.4

 --}}

@php
defined('ABSPATH') || exit;

$show_shipping = !wc_ship_to_billing_address_only() && $order->needs_shipping_address();
@endphp
@if ($show_shipping)

<div class="row">
	<div class="col-lg-6">

		@endif

		<h2 class="woocommerce-column__title">
			{!! esc_html_e('Billing address', 'woocommerce') !!}
		</h2>

		<address>
			{!! wp_kses_post($order->get_formatted_billing_address(esc_html__('N/A', 'woocommerce'))) !!}

			@if ($order->get_billing_phone())
			<p class="woocommerce-customer-details--phone">
				{!! esc_html($order->get_billing_phone()) !!}
			</p>
			@endif

			@if ($order->get_billing_email())
			<p class="woocommerce-customer-details--email">
				{!! esc_html($order->get_billing_email()) !!}
			</p>
			@endif
		</address>

		@if ($show_shipping)

	</div>

	<div class="col-lg-6">
		<h2 class="woocommerce-column__title">
			{!! esc_html_e('Shipping address', 'woocommerce') !!}
		</h2>
		<address>
			{!! wp_kses_post($order->get_formatted_shipping_address(esc_html__('N/A', 'woocommerce'))) !!}
		</address>
	</div>

</div>

@endif

@php
do_action('woocommerce_order_details_after_customer_details', $order);
@endphp
