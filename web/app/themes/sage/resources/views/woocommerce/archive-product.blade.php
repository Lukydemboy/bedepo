{{--
The Template for displaying product archives, including the main shop page which is a post type archive

This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.

HOWEVER, on occasion WooCommerce will need to update template files and you
(the theme developer) will need to copy the new files to your theme to
maintain compatibility. We try to do this as little as possible, but it does
happen. When this occurs the version of the template file will be bumped and
the readme will list any important changes.

@see https://docs.woocommerce.com/document/template-structure/
@package WooCommerce/Templates
@version 3.4.0
--}}

@extends('layouts.app')

@section('content')

@php
	$start_filter = null;

	$cat = get_queried_object();
	if (!empty($cat->taxonomy)) {
		$start_filter = (object) [
			$cat->taxonomy => $cat->slug
		];
	}
@endphp

<section class="pt-md pb-md bg-subtle-white">
	<div class="container">
		<div class="row">
			<div class="col-12 mb-6">
				<h1 class="title">Our animals for sale</h1>
			</div>
			<div class="col-md-5">
				<div class="text">
					These are all our animals that are for sale! We try to update this as soon as possible. If you want te reserve a future offspring of a pair of us, use the contact form and tell us you name, facebook profile and what pair you are interested in. We will put you on the list and keep you updated! If you are interested a specific species of ours, use the filters to make your shopping easier!
				</div>
			</div>
		</div>
	</div>
</section>

<section class="pt-md pb-lg">
	<div class="container">
		@php
		do_action('woocommerce_before_main_content');
		@endphp

		@php
		do_action('woocommerce_archive_description');
		@endphp

		@if(woocommerce_product_loop())
		@php
			do_action('woocommerce_before_shop_loop');
			woocommerce_product_loop_start();
			do_action('woocommerce_shop_loop');
		@endphp

		@if(wc_get_loop_prop('total'))
			@while(have_posts())
				@php
					the_post();
				@endphp
			@endwhile
		@endif
		<product-catalog :start-filter='{{ json_encode($start_filter) }}' />

		@php
			woocommerce_product_loop_end();
			do_action('woocommerce_after_shop_loop');
		@endphp
		@else
			@php
				do_action('woocommerce_no_products_found');
			@endphp
		@endif

		@php
		do_action('woocommerce_after_main_content');
		@endphp
	</div>
</section>
@php
do_action('get_footer', 'shop');
@endphp
@endsection
