@extends('layouts.app')

@section('content')
	@if (!have_posts())
	<div class="container pt-lg pb-lg text-center">
		<h1 class="mt-0 mb-xs">
			{{ __('Page not found.', 'sage') }}
		</h1>
		<a href="{{ get_home_url() }}" class="btn btn--primary">
			{{ __('Back to home', 'sage') }}
		</a>
	</div>
	@endif
@endsection
