{{--
Template Name: Section Builder
--}}

@extends('layouts.app')

@section('content')
	@while(have_posts()) @php the_post() @endphp
		@include('partials.content-page-section-builder')
	@endwhile
@endsection
