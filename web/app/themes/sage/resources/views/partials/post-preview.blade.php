<div class="post-preview">
	<div class="post-preview__image">
		<a href="{{ get_the_permalink($post->ID) }}">
			@php
				$featured_image = get_field('featured_image', $post->ID);
			@endphp
			<img src="{{ $featured_image['url'] }}" alt="{{ $post->post_title }}" loading="lazy">
		</a>
	</div>
	<div class="post-preview__content">

		<h3 class="mt-0 title">
			<a href="{{ get_the_permalink($post->ID) }}">
				{{ get_the_title($post->ID) }}
			</a>
		</h3>

		@php
			$excerpt = strip_tags(get_field('post_description'));
		@endphp
		<div class="text">
			@if (strlen($excerpt) < 240)
				{!! $excerpt !!}
			@else
				{!! substr($excerpt, 0, 240) !!}...
			@endif
		</div>

	</div>
</div>
