<section class="pt-lg pb-lg">
	<div class="container">
		<div class="row">
			@if (!have_posts())
			<div class="alert alert-warning">
				{{ __('Sorry, no results were found.', 'sage') }}
			</div>
			{!! get_search_form(false) !!}
			@endif

			@while (have_posts()) @php the_post() @endphp
			<div class="col-lg-4 col-md-6 mb-4">
				@include('partials.post-preview', [
					'post' => get_post()
				])
			</div>
			@endwhile
		</div>
	</div>
</section>
