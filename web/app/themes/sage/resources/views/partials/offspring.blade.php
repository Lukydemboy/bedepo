<div class="offspring gallery">
	<h2 class="title mb-8">Offspring</h3>

	<div class="row">
		@php
			$offspring = array_map(function($animal) {
				return $animal['image']['url'];
			}, $offspring)
		@endphp
		@foreach ($offspring as $index => $animal)
			<div class="col-md-6 mb-4 {{ $size == 'big' ? 'col-lg-6' : 'col-lg-4' }}">
				<div class="offspring__image">
					<gallery-lightbox :sources="{{ json_encode($offspring) }}" :index="{{ $index }}" preview-image="{{ $animal }}"></gallery-lightbox>
				</div>
			</div>
		@endforeach
	</div>

</div>
