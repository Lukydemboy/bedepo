<header class="header">
	{{-- <div class="top-header hide-mobile">
		<div class="container">
			<div class="d-flex justify-content-end">
				@if (has_nav_menu('secondary_navigation'))
					{!! wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav']) !!}
				@endif
			</div>
		</div>
	</div> --}}
	<div class="primary-header">
		<div class="container">
			<div class="d-flex justify-content-between align-items-center">
				<a class="brand" href="{{ home_url('/') }}">
					<div class="brand__img">
						<img src="{{ get_field('logo', 'options')['url'] }}" alt="Bedepo Logo">
					</div>
				</a>
				<nav class="nav-primary hide-mobile">
					@if (has_nav_menu('primary_navigation'))
						{!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
					@endif
				</nav>
				<div class="mobile-nav hide-tablet-up">
					<div id="navToggler" class="mobile-nav__toggler">
						<div class="line"></div>
						<div class="line"></div>
						<div class="line"></div>
					</div>
					<div id="mobileNav" class="mobile-nav__navigation">
						<div id="navClose" class="mobile-nav__close">
							<svg
								xmlns="http://www.w3.org/2000/svg"
								version="1.1"
								xmlns:xlink="http://www.w3.org/1999/xlink"
								xmlns:svgjs="http://svgjs.com/svgjs"
								viewBox="0 0 140 140"
								width="140"
								height="140"
							>
								<g
									transform="matrix(5.833333333333333,0,0,5.833333333333333,0,0)"
								>
									<path
										d="M0.75 23.249L23.25 0.749"
										fill="none"
										stroke="#949494"
										stroke-linecap="round"
										stroke-linejoin="round"
										stroke-width="1.5"
									></path>
									<path
										d="M23.25 23.249L0.75 0.749"
										fill="none"
										stroke="#949494"
										stroke-linecap="round"
										stroke-linejoin="round"
										stroke-width="1.5"
									></path>
								</g>
							</svg>
						</div>
						<div class="nav-items">
							@if (has_nav_menu('mobile_navigation'))
								{!! wp_nav_menu(['theme_location' => 'mobile_navigation', 'menu_class' => 'nav']) !!}
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
