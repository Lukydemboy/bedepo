@php
$channels = [
'facebook-f',
'instagram',
'twitter',
'youtube',
];
@endphp
<ul class="social-media">
	@foreach ($channels as $channel)
		@if (get_field($channel . '_url', 'options'))
			<li>
				<a href="{{ get_field($channel . '_url', 'options') }}" target="_blank" rel="noopener">
					<i class="fab fa-{{ $channel }}"></i>
				</a>
			</li>
		@endif
	@endforeach
</ul>
