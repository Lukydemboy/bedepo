<div class="parents pb-lg">
	<h2 class="title mb-8">Parents</h2>

	<div class="row">

		@if ($sire)
			<div class="col-md-6">
				<div class="single-product__parent">
					@php
						$dam_image = get_field('featured_image', $dam->ID);
						$sire_image = get_field('featured_image', $sire->ID);
					@endphp
					<div class="parent-image mb-8">
						<img src="{{ $sire_image['url'] }}" alt="{{ $sire->post_title }}">
					</div>
					<h3 class="title">{{ $sire->post_title }}</h3>
					<p class="parent-gender mt-4 mb-2">Sire</p>
					<div class="text">{!! wpautop(get_field('post_description', $sire->ID)) !!}</div>
				</div>
			</div>
		@endif

		@if ($dam)
			<div class="col-md-6">
				<div class="single-product__parent">
					<div class="parent-image mb-8">
						<img src="{{ $dam_image['url'] }}" alt="{{ $dam->post_title }}">
					</div>
					<h3 class="title">{{ $dam->post_title }}</h3>
					<p class="parent-gender mt-4 mb-2">Dam</p>
					<div class="text">{!! wpautop(get_field('post_description', $dam->ID)) !!}</div>
				</div>
			</div>
		@endif

	</div>
</div>
