@php
	if (!isset($show_title_banner)) {
		$show_title_banner = get_field('show_title_banner');
	}
@endphp

@if ($show_title_banner)
	<div class="title-banner pt-md pb-md">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="bread-crumbs">
						{{ breadcrumbs() }}
					</div>
				</div>
				<div class="col-12">
					<div class="title-banner__content">
						<h1 class="title-banner__title">{{ get_the_title() }}</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
@endif
