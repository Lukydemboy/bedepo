<section class="gallery {{ get_sub_field('pt') }} {{ get_sub_field('pb') }} {{ get_sub_field('bg') }}">
	<div class="container">
		<div class="col-12">
			<h2 class="title mb-6">{{ get_sub_field('title') }}</h2>
		</div>
		@php
			$images = get_sub_field('images');
			$images = array_map(function($image) {
				return $image['image']['url'];
			}, $images)
		@endphp
		<div class="row">
			@foreach ($images as $index => $image)
				<div class="col-md-4">
					<gallery-lightbox :sources="{{ json_encode($images) }}" :index="{{ $index }}" preview-image="{{ $image }}"></gallery-lightbox>
				</div>
			@endforeach
		</div>
	</div>
</section>
