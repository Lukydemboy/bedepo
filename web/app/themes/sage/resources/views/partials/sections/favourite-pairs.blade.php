<section class="top-pairs-section">
	<div class="top-pairs__wrapper">
		<div class="img__wrapper {{ get_sub_field('bg') }}">
			@php
			$image = get_sub_field('image');
			@endphp
			<img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}" class="img" loading="lazy">
		</div>
		<div class="top-pairs {{ get_sub_field('pt') }} {{ get_sub_field('pb') }} {{ get_sub_field('bg') }}">
			<div class="pairs__wrapper">
				<h2 class="top-pairs--title">
					{{ get_sub_field('title') }}
				</h2>
				@php
					$pairs = [get_sub_field('pair'), get_sub_field('pair_2'), get_sub_field('pair_3')];
				@endphp
				@foreach ((array)$pairs as $i => $pair)
				@if (is_object($pair))
					<a href="{{ get_permalink($pair->ID) }}">
						<div class="top-pair">
							<div class="top-pair--number pair-{{ $i + 1 }}">
								0{{ $i + 1 }}
							</div>
							<div class="top-pair__info">
								<h3 class="top-pair__info--title">
									{!! $pair->post_title !!}
								</h3>
								<div class="top-pair__info--desc">
									{!! get_field('description', $pair->ID) !!}
								</div>
							</div>
						</div>
					</a>
				@endif
			@endforeach
			</div>
		</div>
	</div>
</section>
