<section class="hero-header {{ get_sub_field('bg') }}">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="content {{ get_sub_field('pt') }} {{ get_sub_field('pb') }}">
					<h1 class="big-title">{{ get_sub_field('title') }}</h1>
					<div class="text">{!! wpautop(get_sub_field('text')) !!}</div>
					@if (get_sub_field('show_button') == '1')
						<a href="{{ get_sub_field('button_link')['url'] }}" class="btn btn--primary">{{ get_sub_field('button_text') }}</a>
					@endif
				</div>
			</div>
			<div class="col-md-8 position-unset">
				<div class="hero-header__image">
					@php
						$image = get_sub_field('image');
					@endphp
					<img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
				</div>
			</div>
		</div>
	</div>
</section>
