<section class="text-section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="text">
					{!! wpautop(get_sub_field('text')) !!}
				</div>
			</div>
		</div>
	</div>
</section>
