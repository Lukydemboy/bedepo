<section class="image {{ get_sub_field('pt') }} {{ get_sub_field('pb') }} {{ get_sub_field('bg') }}">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="image__image">
					<img src="{{ get_sub_field('image')['url'] }}" alt="{{ get_sub_field('image')['alt'] }}" loading="lazy">
				</div>
			</div>
		</div>
	</div>
</section>
