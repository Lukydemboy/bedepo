<section class="image-w-content {{ get_sub_field('pt') }} {{ get_sub_field('pb') }} {{ get_sub_field('bg') }}">
	<div class="container">
		<div class="row {{ get_sub_field('image_position') }}">
			<div class="col-md-4 offset-md-1">
				<div class="content">
					<p class="primary-title">{{ get_sub_field('small_title_above') }}</p>
					<h2 class="title">{{ get_sub_field('title') }}</h2>
					<div class="text">{!! wpautop(get_sub_field('text')) !!}</div>
					@if (get_sub_field('show_button') == '1')
						<a href="{{ get_sub_field('button_link') }}" class="btn btn--primary">{{ get_sub_field('button_text') }}</a>
					@endif
				</div>
			</div>
			<div class="col-md-5 offset-md-1">
				@php
					$image = get_sub_field('image');
				@endphp
				<div class="image">
					<img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}" loading="lazy">
				</div>
			</div>
		</div>
	</div>
</section>
