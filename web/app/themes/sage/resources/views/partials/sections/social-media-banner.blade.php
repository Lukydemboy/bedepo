<section class="social-media-banner {{ get_sub_field('pt') }} {{ get_sub_field('pb') }} {{ get_sub_field('bg') }}">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="content">
					<h2 class="title text-center">{{ get_sub_field('title') }}</h2>
					<div class="text text-center mb-6">{!! wpautop(get_sub_field('text')) !!}</div>
					<div class="socials flex justify-content-center">
						@include('partials.social-media')
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
