<section class="contact-form {{ get_sub_field('pt') }} {{ get_sub_field('pb') }} {{ get_sub_field('bg') }}">
	<div class="container">
		<div class="contact-form__wrapper">
			<div class="row">
				<div class="content__wrapper">
					<div class="col-12">
						<div class="col-md-7">
							<div class="content">
								<h2 class="content__title">{{ get_sub_field('title') }}</h2>
								<div class="content__text w-75 mw-100">{!! wpautop(get_sub_field('text')) !!}</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 offset-md-6 contact-form__form">
					<div class="registration">
						<div class="row">
							<h2 class="title">{{ get_sub_field('form_title') }}</h2>
							<div class="text w-75">
								{!! get_sub_field('form_description') !!}
							</div>
						</div>
						<contact-form></contact-form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
