<section class="new-aninals {{ get_sub_field('pt') }} {{ get_sub_field('pb') }} {{ get_sub_field('bg') }}">
	<div class="container">
		<div class="col-12">
			<div class="d-flex justify-content-between align-items-center pb-md">
				<h2 class="title">{{ get_sub_field('title') }}</h2>
				<a class="btn btn--primary" href="{{ wc_get_page_permalink('shop') }}">Discover all</a>
			</div>
		</div>
		@php
			$latest_products = get_posts([
				'post_type' => 'product',
				'numberposts' => 4,
			]);
		@endphp
		<div class="row">
			@foreach ($latest_products as $product)
				<div class="col-lg-3 col-md-6">
					@include('partials.product-preview', [
						'product' => $product,
					])
				</div>
			@endforeach
		</div>
	</div>
</section>
