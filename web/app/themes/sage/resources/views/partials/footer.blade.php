<footer class="footer">
	<!-- Seperator -->
	<div class="container">
		<div class="footer__separator"></div>
	</div>
	<!-- End Seperator -->

	<!-- Prefooter -->
	<section class="footer__prefooter pt-lg pb-md">
		<div class="container">
			<div class="row mb--sm">
				@php dynamic_sidebar('footer') @endphp
			</div>
		</div>
	</section>
	<!-- End Prefooter -->

	<!-- Seperator -->
	<div class="container">
		<div class="footer__separator"></div>
	</div>
	<!-- End Seperator -->

	<!-- Copyright -->
	<section class="footer__copyright">
		<div class="container d-flex align-items-center justify-content-between">
			<span>
				Copyright {{ \Date('Y') }} {{ get_bloginfo('title') }} -
				Webdesign by us
			</span>

			@include('partials.social-media')
		</div>
	</section>
	<!-- End Copyright -->
</footer>

