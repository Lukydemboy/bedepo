@if(have_rows('sections'))
    @while (have_rows('sections'))
        @php
            the_row();
        @endphp
        @include('partials.sections.' . get_row_layout())
    @endwhile
@endif
