@php
	$thumbnail = get_the_post_thumbnail_url($product->ID, 'full');
	$wc_product = wc_get_product($product->ID);
@endphp
<div class="product-preview">
	<a href="{{ get_permalink($product->ID) }}">
		<div class="product__image">
			@if (get_field('product_label', $product->ID))
				<div class="product__label">{{ get_field('product_label', $product->ID) }}</div>
			@endif
			<img src="{{ get_the_post_thumbnail_url($product->ID) }}" alt="{{ $product->post_title }}" loading="lazy">
		</div>
		<div class="product__description">
			@php
				$regular_price = $wc_product->get_regular_price();
				$sale_price = $wc_product->get_sale_price();
			@endphp
			<div class="d-flex align-items-center justify-content-between">
				<h2 class="product__title">{{ $product->post_title }}</h2>
				@if ($sale_price)
					<p class="product__price">€ {{ number_format($sale_price, 2, '.', ' ') }}</p>
				@else
					<p class="product__price">€ {{ number_format($regular_price, 2, '.', ' ') }}</p>
				@endif
			</div>
			<div class="d-flex align-items-center justify-content-between">
				<p class="product__species">{{ $wc_product->get_attribute( 'pa_sex' ) ? $wc_product->get_attribute( 'pa_sex' ) : '' }}</p>
				@if ($sale_price)
					<p class="product__price on-sale">€ {{ number_format($regular_price, 2, '.', ' ') }}</p>
				@endif
			</div>
			@php
				$excerpt = strip_tags($wc_product->get_description());
			@endphp
			<div class="product__excerpt text">{!! strlen($excerpt) <= 175 ? $excerpt : substr($excerpt, 0, 175) . '...' !!}</div>
		</div>
	</a>
</div>
