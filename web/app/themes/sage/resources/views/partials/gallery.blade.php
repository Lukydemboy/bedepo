<section class="gallery">
	<div class="col-12">
		<h2 class="title mb-6">Gallery</h2>
	</div>
	@php
		$images = array_map(function($image) {
			return $image['image']['url'];
		}, $images)
	@endphp
	<div class="row">
		@foreach ($images as $index => $image)
			<div class="col-md-4 mb-4 {{ $size == 'big' ? 'col-lg-6' : 'col-lg-4' }}">
				<gallery-lightbox :sources="{{ json_encode($images) }}" :index="{{ $index }}" preview-image="{{ $image }}"></gallery-lightbox>
			</div>
		@endforeach
	</div>
</section>
