<div class="animal-sidebar">
	<div class="animal-sidebar__image mb-6">
		@if (get_field('sidebar_image'))
			<img src="{{ get_field('sidebar_image', get_the_id())['url'] }}" alt="{{  get_field('sidebar_image', get_the_id())['alt'] }}">
		@else
			<img src="{{ get_field('featured_image', get_the_id())['url'] }}" alt="{{  get_field('featured_image', get_the_id())['alt'] }}">
		@endif
	</div>
	<div class="animal-sidebar__content p-4 px-6">
		<div class="d-flex align-items-center justify-content-between">
			<h2 class="title">{{ $post->post_title }}</h2>
			<div class="animal-sidebar__gender mb-2">
				@if (get_field('gender', get_the_ID()) == 'male')
					<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" viewBox="0 0 22 22" width="22" height="22"><g transform="matrix(0.9166666666666666,0,0,0.9166666666666666,0,0)"><path d="M24,.5a.5.5,0,0,0-.5-.5h-7a.5.5,0,0,0-.354.854l2.44,2.439a.25.25,0,0,1,0,.354L14.132,8.1a.25.25,0,0,1-.322.026,8.782,8.782,0,1,0,2.063,2.063.25.25,0,0,1,.026-.322l4.455-4.454a.25.25,0,0,1,.353,0l2.439,2.44A.5.5,0,0,0,24,7.5ZM8.75,21.5A6.25,6.25,0,1,1,15,15.25,6.257,6.257,0,0,1,8.75,21.5Z" fill="#1d86df" stroke="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="0"></path><path d="M24,.5a.5.5,0,0,0-.5-.5h-7a.5.5,0,0,0-.354.854l2.44,2.439a.25.25,0,0,1,0,.354L14.132,8.1a.25.25,0,0,1-.322.026,8.782,8.782,0,1,0,2.063,2.063.25.25,0,0,1,.026-.322l4.455-4.454a.25.25,0,0,1,.353,0l2.439,2.44A.5.5,0,0,0,24,7.5ZM8.75,21.5A6.25,6.25,0,1,1,15,15.25,6.257,6.257,0,0,1,8.75,21.5Z" fill="#1d86df" stroke="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="0"></path></g></svg>
				@endif
				@if (get_field('gender', get_the_ID()) == 'female')
					<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" viewBox="0 0 22 22" width="22" height="22"><g transform="matrix(0.9166666666666666,0,0,0.9166666666666666,0,0)"><path d="M19.75,7.75a7.75,7.75,0,1,0-9.2,7.6.249.249,0,0,1,.2.245V18.25a.25.25,0,0,1-.25.25H9A1.25,1.25,0,0,0,9,21h1.5a.25.25,0,0,1,.25.25v1.5a1.25,1.25,0,0,0,2.5,0v-1.5A.25.25,0,0,1,13.5,21H15a1.25,1.25,0,0,0,0-2.5H13.5a.25.25,0,0,1-.25-.25V15.6a.249.249,0,0,1,.2-.245A7.756,7.756,0,0,0,19.75,7.75Zm-13,0A5.25,5.25,0,1,1,12,13,5.256,5.256,0,0,1,6.75,7.75Z" fill="#f78bae" stroke="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="0"></path><path d="M19.75,7.75a7.75,7.75,0,1,0-9.2,7.6.249.249,0,0,1,.2.245V18.25a.25.25,0,0,1-.25.25H9A1.25,1.25,0,0,0,9,21h1.5a.25.25,0,0,1,.25.25v1.5a1.25,1.25,0,0,0,2.5,0v-1.5A.25.25,0,0,1,13.5,21H15a1.25,1.25,0,0,0,0-2.5H13.5a.25.25,0,0,1-.25-.25V15.6a.249.249,0,0,1,.2-.245A7.756,7.756,0,0,0,19.75,7.75Zm-13,0A5.25,5.25,0,1,1,12,13,5.256,5.256,0,0,1,6.75,7.75Z" fill="#f78bae" stroke="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="0"></path></g></svg>
				@endif
				@if (get_field('gender', get_the_ID()) == 'unsexed')
					<svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" viewBox="0 0 22 22" width="22" height="22"><g transform="matrix(0.9166666666666666,0,0,0.9166666666666666,0,0)"><path d="M22,.75a.5.5,0,0,0-.5-.5H18.471a.5.5,0,0,0-.365.842l.739.79a.25.25,0,0,1,0,.346l-2.7,2.744A.25.25,0,0,1,15.816,5,6.455,6.455,0,0,0,8.183,5a.247.247,0,0,1-.325-.027L7.6,4.714a.25.25,0,0,1,0-.352l.671-.673A1,1,0,0,0,6.862,2.275l-.656.657a.247.247,0,0,1-.178.073.25.25,0,0,1-.177-.074l-.692-.7a.25.25,0,0,1,0-.346l.74-.79A.5.5,0,0,0,5.529.25H2.5a.5.5,0,0,0-.5.5V3.985a.5.5,0,0,0,.865.342l.58-.619a.248.248,0,0,1,.18-.079.251.251,0,0,1,.181.074l.634.645a.25.25,0,0,1,0,.352L4,5.143A1,1,0,0,0,5.41,6.557l.427-.427a.249.249,0,0,1,.355,0l.3.308a.251.251,0,0,1,.033.311A6.486,6.486,0,0,0,10.8,16.63a.25.25,0,0,1,.2.245V19a.25.25,0,0,1-.25.25H9.411a1,1,0,0,0,0,2H10.75a.25.25,0,0,1,.25.25v1.25a1,1,0,0,0,2,0V21.5a.25.25,0,0,1,.25-.25h1.338a1,1,0,1,0,0-2H13.25A.25.25,0,0,1,13,19V16.875a.25.25,0,0,1,.2-.245,6.487,6.487,0,0,0,4.268-9.881.249.249,0,0,1,.032-.31L20.194,3.7a.251.251,0,0,1,.181-.074.248.248,0,0,1,.18.079l.58.619A.5.5,0,0,0,22,3.985Zm-10,14a4.5,4.5,0,1,1,4.5-4.5A4.505,4.505,0,0,1,12,14.75Z" fill="#803a7d" stroke="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="0"></path><path d="M22,.75a.5.5,0,0,0-.5-.5H18.471a.5.5,0,0,0-.365.842l.739.79a.25.25,0,0,1,0,.346l-2.7,2.744A.25.25,0,0,1,15.816,5,6.455,6.455,0,0,0,8.183,5a.247.247,0,0,1-.325-.027L7.6,4.714a.25.25,0,0,1,0-.352l.671-.673A1,1,0,0,0,6.862,2.275l-.656.657a.247.247,0,0,1-.178.073.25.25,0,0,1-.177-.074l-.692-.7a.25.25,0,0,1,0-.346l.74-.79A.5.5,0,0,0,5.529.25H2.5a.5.5,0,0,0-.5.5V3.985a.5.5,0,0,0,.865.342l.58-.619a.248.248,0,0,1,.18-.079.251.251,0,0,1,.181.074l.634.645a.25.25,0,0,1,0,.352L4,5.143A1,1,0,0,0,5.41,6.557l.427-.427a.249.249,0,0,1,.355,0l.3.308a.251.251,0,0,1,.033.311A6.486,6.486,0,0,0,10.8,16.63a.25.25,0,0,1,.2.245V19a.25.25,0,0,1-.25.25H9.411a1,1,0,0,0,0,2H10.75a.25.25,0,0,1,.25.25v1.25a1,1,0,0,0,2,0V21.5a.25.25,0,0,1,.25-.25h1.338a1,1,0,1,0,0-2H13.25A.25.25,0,0,1,13,19V16.875a.25.25,0,0,1,.2-.245,6.487,6.487,0,0,0,4.268-9.881.249.249,0,0,1,.032-.31L20.194,3.7a.251.251,0,0,1,.181-.074.248.248,0,0,1,.18.079l.58.619A.5.5,0,0,0,22,3.985Zm-10,14a4.5,4.5,0,1,1,4.5-4.5A4.505,4.505,0,0,1,12,14.75Z" fill="#803a7d" stroke="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="0"></path></g></svg>
				@endif
			</div>
		</div>
		@foreach (get_field('species', $post->ID) as $id)
			<h3 class="primary-title text-capitalize">{{ get_term($id)->name }}</h3>
		@endforeach
		<div class="mb-4">
			<p class="sidebar-title mb-0">Bred by</p>
			<p class="text text--small">{{ get_field('bred_by') }}</p>
		</div>
		<div class="d-flex">
			@if (get_field('sire_name', $post->ID))
				<div class="mb-4 w-50">
					<p class="sidebar-title mb-0">Sire</p>
					<p class="text text--small"><a href="{{ get_field('sire_url', $post->ID) }}" target="_blank">{{ get_field('sire_name', $post->ID) }}</a></p>
				</div>
			@elseif (get_field('dam_name', $post->ID))
				<div class="mb-4 w-50">
					<p class="sidebar-title mb-0">Dam</p>
					<p class="text text--small"><a href="{{ get_field('dam_url', $post->ID) }}" target="_blank">{{ get_field('dam_name', $post->ID) }}</a></p>
				</div>
			@endif
		</div>
		@if (get_field('morph', $post->ID))
			<div class="mb-4">
				<p class="sidebar-title mb-0">Morph</p>
				<p class="text text--small">{{ get_field('morph', $post->ID) }}</p>
			</div>
		@endif
		@if (get_field('weight', $post->ID))
			<div class="mb-4">
				<p class="sidebar-title mb-0">Weight</p>
				<p class="text text--small">{{ get_field('weight', $post->ID) }}</p>
			</div>
		@endif
	</div>
</div>
