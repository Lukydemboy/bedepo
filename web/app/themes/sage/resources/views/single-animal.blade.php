@extends('layouts.app')

@section('content')
	@include('partials.title-banner', [
		'show_title_banner' => 1,
	])

	<div class="animal-content pb-md">
		<div class="container">
			<div class="row justify-content-between m-column-reverse">
				<div class="col-lg-7">
					<!-- Content -->
					<article class="animal-content__article pb-sm">
						@if (get_field('featured_image', get_the_id()))
							<div class="image__image mb-6">
								<img src="{{ get_field('featured_image', get_the_id())['url'] }}" alt="{{  get_field('featured_image', get_the_id())['alt'] }}">
							</div>
						@endif
						<div class="text">
							{!! wpautop(get_field('post_description', get_the_id())) !!}
						</div>
					</article>
					<!-- End Content -->

					<!-- Gallery -->
					@if (get_field('images', get_the_ID()))
						@include('partials.gallery', [
							'images' => get_field('images', get_the_ID()),
							'size' => 'big'
						])
					@endif
					<!-- End Gallery -->

					@if(have_rows('sections'))
						@while (have_rows('sections'))
							@php
								the_row();
							@endphp
							@include('partials.sections.' . get_row_layout())
						@endwhile
					@endif

					<!-- Offspring -->
					@if (get_field('offspring', get_the_ID()))
						@include('partials.offspring', [
							'offspring' => get_field('offspring', get_the_ID()),
							'size' => 'big'
						])
					@endif
					<!-- End Offspring -->

				</div>
				<div class="col-lg-4 mb-4">
					<!-- Sidebar -->
					<div class="sidebar animal-content__sidebar">
						@include('partials.animal-sidebar', [
						'post' => get_post()
						])
					</div>
					<!-- End Sidebar -->
				</div>
			</div>

		</div>
	</div>
@endsection
