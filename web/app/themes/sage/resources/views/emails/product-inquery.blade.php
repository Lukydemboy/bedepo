@extends('layouts.email')

@section('preview')
Iemand toonde interesse in {{ $name }}
@endsection

@section('content')
<p>
	Hallo
</p>

<p>
	Iemand toonde interesse in {{ $name }}
</p>

<table style="width: 100%;">
	<tr>
		<td>
			<strong>{{ __('name', 'sage') }}</strong>
		</td>
		<td>
			{{ $name }}
		</td>
	</tr>
	<tr>
		<td>
			<strong>{{ __('Email', 'sage') }}</strong>
		</td>
		<td>
			{{ $email }}
		</td>
	</tr>
	<tr>
		<td>
			<strong>{{ __('Animal', 'sage') }}</strong>
		</td>
		<td>
			{{ $animal }}
		</td>
	</tr>
	<tr>
		<td>
			<strong>{{ __('Animal URL', 'sage') }}</strong>
		</td>
		<td>
			{{ $animal_url }}
		</td>
	</tr>
</table>

{!! wpautop($message) !!}

@endsection
