@extends('layouts.email')

@section('preview')
Nieuw bericht via contactformulier
@endsection

@section('content')
<p>
	Hallo
</p>

<p>
	Er werd zonet een nieuw bericht verstuurd via het contactformulier.
</p>

<table style="width: 100%;">
	<tr>
		<td>
			<strong>{{ __('name', 'sage') }}</strong>
		</td>
		<td>
			{{ $fname }} {{ $lname }}
		</td>
	</tr>
	<tr>
		<td>
			<strong>{{ __('Email', 'sage') }}</strong>
		</td>
		<td>
			{{ $email }}
		</td>
	</tr>
</table>

{!! wpautop($message) !!}

@endsection
