@extends('layouts.app')

@section('content')
	@include('partials.title-banner', [
		'show_title_banner' => true,
	])

	<div class="pair pt-lg pb-xl">
		<div class="container">
			@php
				$sire = get_field('sire', get_the_ID());
				$dam = get_field('dam', get_the_ID());
			@endphp

			<!-- Parents -->
			@if ($sire || $dam)
				@include('partials.parents', [
					'sire' => $sire,
					'dam' => $dam,
				])
			@endif
			<!-- End Parents -->

			<!-- Offspring -->
			@if (get_field('offspring'))
				@include('partials.offspring', [
					'offspring' => get_field('offspring'),
				])
			@endif
			<!-- End Offspring -->

		</div>
	</div>
@endsection
