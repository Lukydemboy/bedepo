<!doctype html>
<html {!! get_language_attributes() !!}>
	@include('partials.head')
	<body @php body_class() @endphp>
		@include('partials.header')

		<div id="app" class="content">
			<main class="main">
				@include('partials.title-banner')
				@yield('content')
			</main>
		</div>

		@php do_action('get_footer') @endphp
		@include('partials.footer')

		@php wp_footer() @endphp
	</body>
</html>
