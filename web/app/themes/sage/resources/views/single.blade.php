@extends('layouts.app')

@section('content')
	@include('partials.title-banner', [
		'show_title_banner' => 1,
	])
	<div class="text-section">
		<section class="image pt-none pb-md bg-subtle-white">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="image__image">
							<img src="{{ get_field('featured_image', get_the_id())['url'] }}" alt="{{  get_field('featured_image', get_the_id())['alt'] }}">
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="container">
			<div class="text">
				{!! wpautop(get_field('post_description', get_the_id())) !!}
			</div>
		</div>
	</div>

	@while(have_posts()) @php the_post() @endphp
		@include('partials.content-page-section-builder')
	@endwhile

	<div class="container">
		<div class="row">
			<!-- Offspring -->
			@if (get_field('offspring', get_the_ID()))
				@include('partials.offspring', [
					'offspring' => get_field('offspring', get_the_ID()),
				])
			@endif
			<!-- End Offspring -->
		</div>
	</div>

@endsection
