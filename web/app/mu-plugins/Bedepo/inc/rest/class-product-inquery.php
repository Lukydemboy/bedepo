<?php
namespace Bedepo\REST;

use Bedepo\Inc\Smtp_Mailer;
use Valitron\Validator;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

new ProductInquery;

class ProductInquery
{
    /**
     * Constructor
     */
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'register_rest_routes']);
    }

    /**
     * Register REST routes
     */
    public function register_rest_routes()
    {
        register_rest_route( 'bedepo', '/product-inquery', [
            'methods' => 'POST',
            'callback' => [$this, 'handle_form_submission'],
            'permission_callback' => '__return_true'
        ]);
    }

    /**
     * Handle form submission
     */
    public function handle_form_submission($request)
    {
        // Create validator instance
        $v = new Validator($request->get_params());

        // Sanitize params
        $sanitized = $request->sanitize_params();
        if (is_wp_error($sanitized)) {
            return new \WP_REST_Response([
                'message' => 'Something went wrong processing your data. Please try again later.',
                'errors' => $v->errors()
            ], 422);
        }

        // Define labels
        $v->labels([
            'name' => 'Name',
            'email' => 'Email',
            'message' => 'Message',
        ]);

        // Define validation rules
        $v->rule('required', [
            'name',
            'email',
            'message',
        ]);

        // Bail if the data doesn't pass the validation
        if (!$v->validate()) {
            return new \WP_REST_Response([
                'message' => 'Your message could not be sent. Please fill out all the required fields and try again.',
                'errors' => $v->errors()
            ], 422);
        }

        // Compose mail
        $html = \App\template('emails.product-inquery', [
            'name' => $request->get_param('name'),
            'email' => $request->get_param('email'),
            'message' => $request->get_param('message'),
            'animal' => $request->get_param('animal'),
            'animal_url' => $request->get_param('animalUrl'),
        ]);

        // Inline styles
        $content = (new CssToInlineStyles())->convert($html);

        // Sent the email
        $mail_sent = wp_mail(get_field('email', 'options'), 'Someone wants to buy an animal!', $content, [
            'Content-Type: text/html; charset=UTF-8',
            'Reply-To: ' . $request->get_param('email')
        ]);

        // Bail if mail couldn't be sent
        if (!$mail_sent) {
            return new \WP_REST_Response([
                'message' => 'Your message could not be sent due to a technical error. Please try again later.',
                'errors' => []
            ], 500);
        }

        // Return success response
        return new \WP_REST_Response([
            'message' => 'Thank you for your message! We will get back to you as soon as possible.',
        ], 200);
    }
}
