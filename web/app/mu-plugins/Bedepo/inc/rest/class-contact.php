<?php
namespace Bedepo\REST;

use Bedepo\Inc\Smtp_Mailer;
use Valitron\Validator;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

new Contact;

class Contact
{
    /**
     * Constructor
     */
    public function __construct()
    {
        add_action('rest_api_init', [$this, 'register_rest_routes']);
    }

    /**
     * Register REST routes
     */
    public function register_rest_routes()
    {
        register_rest_route( 'bedepo', '/contact', [
            'methods' => 'POST',
            'callback' => [$this, 'handle_form_submission'],
            'permission_callback' => '__return_true'
        ]);
    }

    /**
     * Handle form submission
     */
    public function handle_form_submission($request)
    {
        // Sanitize params
        $sanitized = $request->sanitize_params();
        if (is_wp_error($sanitized)) {
            return new \WP_REST_Response([
                'message' => 'Something went wrong processing your data. Please try again later.',
                'errors' => $v->errors()
            ], 422);
        }

        // Create validator instance
        $v = new Validator($request->get_params());

        // Define labels
        $v->labels([
            'fname' => 'First Name',
            'lname' => 'Last Name',
            'email' => 'Email',
            'message' => 'Message',
            'gdpr' => 'Privacy policy',
        ]);

        // Define validation rules
        $v->rule('required', [
            'fname',
            'lname',
            'email',
            'message',
        ]);

        $v->rule('accepted', [
            'gdpr',
        ]);

        // Bail if the data doesn't pass the validation
        if (!$v->validate()) {
            return new \WP_REST_Response([
                'message' => 'Your message could not be sent. Please fill out all the required fields and try again.',
                'errors' => $v->errors()
            ], 422);
        }

        // Compose mail
        $html = \App\template('emails.contact', [
            'fname' => $request->get_param('fname'),
            'lname' => $request->get_param('lname'),
            'email' => $request->get_param('email'),
            'message' => $request->get_param('message'),
        ]);

        // Inline styles
        $content = (new CssToInlineStyles())->convert($html);

        // Sent the email
        $mail_sent = wp_mail(get_field('email', 'options'), 'New message via contact form', $content, [
            'Content-Type: text/html; charset=UTF-8',
            'Reply-To: ' . $request->get_param('email')
        ]);

        // Bail if mail couldn't be sent
        if (!$mail_sent) {
            return new \WP_REST_Response([
                'message' => 'Your message could not be sent due to a technical error. Please try again later.',
                'errors' => []
            ], 500);
        }

        // Return success response
        return new \WP_REST_Response([
            'message' => 'Thank you for your message! We will get back to you as soon as possible.',
        ], 200);
    }
}
