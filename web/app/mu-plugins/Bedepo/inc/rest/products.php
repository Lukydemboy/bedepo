<?php

namespace Bedepo\REST\Products;

add_action('rest_api_init', function () {
    register_rest_route('bedepo', '/products', [
        'methods' => 'POST',
        'callback' => __NAMESPACE__ . '\get_products',
        'permission_callback' => '__return_true'
    ]);

    register_rest_route('bedepo', '/products/filters', [
        'methods' => 'GET',
        'callback' => __NAMESPACE__ . '\get_product_filters',
        'permission_callback' => '__return_true'
    ]);
});

/**
 * Get products
 */
function get_products($request)
{
    $args = [
        'post_type' => 'product',
        'posts_per_page' => 20,
        'paged' => $request->get_param('page') ?: 1,
    ];

    switch ($request->get_param('sort')) {
        case 'z-a':
            $args['orderby'] = 'title';
            $args['order'] = 'DESC';
            break;
        case 'price-asc':
            $args['meta_key'] = '_price';
            $args['orderby'] = 'meta_value_num';
            $args['order'] = 'ASC';
            break;
        case 'price-desc':
            $args['meta_key'] = '_price';
            $args['orderby'] = 'meta_value_num';
            $args['order'] = 'DESC';
            break;
        default:
            $args['orderby'] = 'title';
            $args['order'] = 'ASC';
            break;
    }

    if ($request->get_param('filters')) {
        $filters = $request->get_param('filters');

        $args['tax_query'] = [];
        foreach ($filters as $taxonomy => $value) {
            if ($taxonomy == 'price') {
                $args['meta_query'] = [];
                $args['meta_query'] = [
                    'relation' => 'OR'
                ];

                list($min, $max) = explode('-', $value);
                $args['meta_query'][] = [
                    'key' => '_price',
                    'value' => explode('-', $value),
                    'compare' => 'BETWEEN',
                    'type' => 'NUMERIC'
                ];
            } else {
                $args['tax_query'][] = [
                    'taxonomy' => $taxonomy == 'product_cat' ? 'product_cat' : 'pa_' . $taxonomy,
                    'field' => 'slug',
                    'terms' => [$value]
                ];
            }
        }
    }

    // die(print_r($args));

    return array_map(function ($post) {
        $product = new \stdClass();
        $product->id = $post->ID;
        $product->title = $post->post_title;
        $product->url = get_the_permalink($post->ID);
        $product->thumbnail = has_post_thumbnail($post->ID) ? get_the_post_thumbnail_url($post->ID) : null;

        // Retrieve WC product
        $wc_product = wc_get_product($post->ID);

        $product->excerpt = strip_tags($wc_product->get_description());;
        $product->price = $wc_product->get_price();
        $product->sale_price = $wc_product->get_sale_price();
        $product->regular_price = $wc_product->get_regular_price();
        $product->sex = $wc_product->get_attribute( 'pa_sex' );

        $product->label = get_field('product_label', $post->ID);

        return $product;
    }, (array)get_posts($args));
}

/**
 * Get product filters
 */
function get_product_filters($request)
{

    $filters = [];

    // Add categories to filters

    //     'slug' => 'product_cat',
    //     'name' => 'Soort',
    //     'options' => array_values(array_map(function ($term) {
    //         $category = new \stdClass();

    //         $category->id = $term->term_id;
    //         $category->slug = $term->slug;
    //         $category->name = $term->name;

    //         return $category;
    //     }, (array)get_terms([
    //         'taxonomy' => 'product_cat',
    //     ]))),

    // Add enabled product filters
    $enabled_filters = get_field('product_filters', 'option');
    foreach ((array)$enabled_filters as $enabled_filter) {
        $filters[] = [
            'slug' => $enabled_filter['value'],
            'name' => $enabled_filter['label'],
            'options' => array_map(function ($term) {
                $category = new \stdClass();

                $category->id = $term->term_id;
                $category->slug = $term->slug;
                $category->name = $term->name;

                return $category;
            }, (array)get_terms([
                'taxonomy' => 'pa_' . $enabled_filter['value'],
            ])),
        ];
    }

    // $filters[] = [
    //     'slug' => 'price',
    //     'name' => 'Prijs',
    //     'options' => array_map(function ($range) {
    //         $filter = new \stdClass();

    //         $filter->id = null;
    //         $filter->slug = implode('-', [
    //             $range['min'],
    //             $range['max'],
    //         ]);
    //         $filter->name = implode(' - ', [
    //             number_format($range['min'], 2, ',', '.'),
    //             number_format($range['max'], 2, ',', '.'),
    //         ]);

    //         return $filter;
    //     }, (array)get_field('price_ranges', 'options')),
    // ];

    return $filters;
}
