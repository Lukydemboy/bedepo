<?php

namespace Bedepo\REST\Faq;

add_action( 'rest_api_init', function () {
	register_rest_route( 'bedepo', '/getCategoryPost', [
        'methods' => 'POST',
        'callback' => __NAMESPACE__ . '\getCategoryPosts',
	]);
});

add_action( 'rest_api_init', function () {
	register_rest_route( 'bedepo', '/getCategories', [
        'methods' => 'GET',
        'callback' => __NAMESPACE__ . '\getCategories',
	]);
});

function getCategoryPosts($request) {

	$posts = get_posts([
		'post_type' => 'faq',
		'posts_per_page' => -1,
		'tax_query' => [
			[
				'taxonomy' => 'category',
				'field', 'id',
				'terms' => [$request->get_param('id')],
			]
		]
	]);

	return $posts;
}

function getCategories() {

	$category_id = get_cat_ID('faq');

	$args = [
		'parent' => $category_id,
        'hide_empty' => 1,
	];

	return array_map(function ($category) {
        return $category;
    }, get_categories($args));
}
