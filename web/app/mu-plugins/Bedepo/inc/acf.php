<?php

/**
 * Create options page
 */
if( function_exists('acf_add_options_page') ) {
    // General options
	acf_add_options_page([
        'page_title' => 'General Settings',
		'menu_title' => 'General Settings',
		'menu_slug' => 'theme-general-settings',
		'capability' => 'edit_posts',
        'redirect' => false,
        'autoload' => true
    ]);
}

/**
 * Save ACF fields to local JSON
 */
add_filter('acf/settings/save_json', function ($path) {
	$path = __DIR__ . '/../fields';
	return $path;
});

/**
 * Load ACF fields from local JSON
 */
add_filter('acf/settings/load_json', function($paths) {
	unset($paths[0]);
	$paths[] = __DIR__ . '/../fields';
	return $paths;
});


/**
 * Dynamically load product filter choices
 */
add_filter('acf/load_field/name=product_filters', function($field) {
    $taxonomies = wc_get_attribute_taxonomies();

    foreach((array)$taxonomies as $taxonomy) {
        $field['choices'][$taxonomy->attribute_name] = $taxonomy->attribute_label;
    }

    return $field;
});

