<?php

add_filter('upload_mimes', function($mimes) {
    $mimes['webp'] = 'image/webp';
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
});
