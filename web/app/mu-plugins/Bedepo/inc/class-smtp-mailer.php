<?php

namespace Mix\Inc;

use PHPMailer\PHPMailer\PHPMailer;

new Smtp_Mailer;

class Smtp_Mailer
{
    /**
     * Constructor
     */
    public function __construct()
    {
        // Hooks
        add_action('phpmailer_init', [$this, 'set_smtp_settings']);
    }

    /**
     * Set SMTP settings
     */
    public function set_smtp_settings(PHPMailer $phpmailer)
    {
        $phpmailer->Host = getenv('SMTP_HOST');
        $phpmailer->Port = getenv('SMTP_PORT');
        $phpmailer->SMTPAuth = true;
        $phpmailer->Username = getenv('SMTP_USERNAME');
        $phpmailer->Password = getenv('SMTP_PASSWORD');
        $phpmailer->setFrom(getenv('SMTP_FROM_EMAIL'), getenv('SMTP_FROM'));
        $phpmailer->isSMTP();
    }
}
