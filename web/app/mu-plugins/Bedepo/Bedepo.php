<?php

/**
 * Plugin Name: Bedepo
 * Description: Functionality plugin.
 * Author: Lukas Demeyere
 * Author Uri:
 * Version: 0.1
 * Text Domain: bedepo
 */

require_once(dirname(__FILE__) . '/inc/acf.php');
require_once(dirname(__FILE__) . '/inc/mime-types.php');
require_once(dirname(__FILE__) . '/inc/breadcrumbs.php');
require_once(dirname(__FILE__) . '/inc/class-smtp-mailer.php');

require_once(dirname(__FILE__) . '/post-types/faq.php');
require_once(dirname(__FILE__) . '/post-types/pairs.php');
require_once(dirname(__FILE__) . '/post-types/animals.php');
require_once(dirname(__FILE__) . '/post-types/caresheets.php');

require_once(dirname(__FILE__) . '/inc/rest/faq.php');
require_once(dirname(__FILE__) . '/inc/rest/products.php');
require_once(dirname(__FILE__) . '/inc/rest/class-contact.php');
require_once(dirname(__FILE__) . '/inc/rest/class-product-inquery.php');


