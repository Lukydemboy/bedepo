<?php
add_action('init', function()
{
    $labels = [
        'name'                => _x("FAQ's", 'Post Type General Name'),
        'singular_name'       => _x('FAQ', 'Post Type Singular Name'),
        'menu_name'           => __("FAQ's"),
        'parent_item_colon'   => __('Parent FAQ'),
        'all_items'           => __("All FAQ's"),
        'view_item'           => __('Watch FAQ'),
        'add_new_item'        => __('Add FAQ'),
        'add_new'             => __('Add FAQ'),
        'edit_item'           => __('Edit FAQ'),
        'update_item'         => __('Update FAQ'),
        'search_items'        => __('Search FAQ'),
        'not_found'           => __('Not found'),
        'not_found_in_trash'  => __('Not found in thrash'),
    ];
    $args = [
        'label'               => __('faq'),
        'description'         => __('faq'),
        'labels'              => $labels,
        'supports'            => ['title', 'editor'],
        'hierarchical'        => false,
        'public'              => true,
        'menu_icon'           => 'dashicons-editor-help',
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 4,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'rewrite' => [
            'slug' => 'faq'
        ],
        'taxonomies'  => array( 'category' ),
    ];
    register_post_type('faq', $args);
});
