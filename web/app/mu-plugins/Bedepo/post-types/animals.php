<?php
add_action('init',  function() {
	$labels  =  array(
		'name'  =>  _x("Animals",  'Post Type General Name'),
		'singular_name'  =>  _x('Animal',  'Post Type Singular Name'),
		'menu_name'  =>  __("Animals"),
		'parent_item_colon'  =>  __('Parent Locatie'),
		'all_items'  =>  __("Alle animals"),
		'view_item'  =>  __('Animal bekijken'),
		'add_new_item'  =>  __('Nieuwe animal toevoegen'),
		'add_new'  =>  __('Nieuwe toevoegen'),
		'edit_item'  =>  __('Animal bewerken'),
		'update_item'  =>  __('Animal updaten'),
		'search_items'  =>  __('Animal zoeken'),
		'not_found'  =>  __('Niet gevonden'),
		'not_found_in_trash'  =>  __('Niet gevonden in prullenbak'),
	);

	$args  =  array(
		'label'  =>  __('Animals'),
		'description'  =>  __('Animals'),
		'labels'  =>  $labels,
		'supports'  =>  ['title', 'editor'],
		'hierarchical'  =>  false,
		'public'  =>  true,
		'menu_icon'  =>  'dashicons-pets',
		'show_ui'  =>  true,
		'show_in_menu'  =>  true,
		'show_in_nav_menus'  =>  true,
		'show_in_admin_bar'  =>  true,
		'menu_position'  =>  4,
		'can_export'  =>  true,
		'has_archive'  =>  true,
		'exclude_from_search'  =>  false,
		'publicly_queryable'  =>  true,
		'capability_type'  =>  'post',
        'taxonomies' => ['category'],
	);

	register_post_type('animal', $args);
}, 0, 20);
