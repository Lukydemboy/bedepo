<?php
add_action('init',  function() {
	$labels  =  array(
		'name'  =>  _x("Caresheets",  'Post Type General Name'),
		'singular_name'  =>  _x('Caresheet',  'Post Type Singular Name'),
		'menu_name'  =>  __("Caresheets"),
		'parent_item_colon'  =>  __('Parent Location'),
		'all_items'  =>  __("All caresheets"),
		'view_item'  =>  __('View caresheet'),
		'add_new_item'  =>  __('Add caresheet'),
		'add_new'  =>  __('Add new'),
		'edit_item'  =>  __('Edit caresheet'),
		'update_item'  =>  __('Update caresheet'),
		'search_items'  =>  __('Search caresheet'),
		'not_found'  =>  __('Not found'),
		'not_found_in_trash'  =>  __('Not found in thrash'),
	);

	$args  =  array(
		'label'  =>  __('Caresheets'),
		'description'  =>  __('Caresheets on how to care about animals'),
		'labels'  =>  $labels,
		'supports'  =>  ['title', 'editor'],
		'hierarchical'  =>  false,
		'public'  =>  true,
		'menu_icon'  =>  'dashicons-text-page',
		'show_ui'  =>  true,
		'show_in_menu'  =>  true,
		'show_in_nav_menus'  =>  true,
		'show_in_admin_bar'  =>  true,
		'menu_position'  =>  4,
		'can_export'  =>  true,
		'has_archive'  =>  true,
		'exclude_from_search'  =>  false,
		'publicly_queryable'  =>  true,
		'capability_type'  =>  'post',
        'taxonomies' => ['category'],
	);

	register_post_type('caresheet', $args);
}, 0, 20);
