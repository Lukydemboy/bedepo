<?php
add_action('init',  function() {
	$labels  =  array(
		'name'  =>  _x("Pairs",  'Post Type General Name'),
		'singular_name'  =>  _x('Pair',  'Post Type Singular Name'),
		'menu_name'  =>  __("Pairs"),
		'parent_item_colon'  =>  __('Parent Locatie'),
		'all_items'  =>  __("Alle Pairs"),
		'view_item'  =>  __('Pair bekijken'),
		'add_new_item'  =>  __('Nieuwe pair toevoegen'),
		'add_new'  =>  __('Nieuwe toevoegen'),
		'edit_item'  =>  __('Pair bewerken'),
		'update_item'  =>  __('Pair updaten'),
		'search_items'  =>  __('Pair zoeken'),
		'not_found'  =>  __('Niet gevonden'),
		'not_found_in_trash'  =>  __('Niet gevonden in prullenbak'),
	);

	$args  =  array(
		'label'  =>  __('Pairs'),
		'description'  =>  __('Pairs'),
		'labels'  =>  $labels,
		'supports'  =>  ['title', 'editor'],
		'hierarchical'  =>  false,
		'public'  =>  true,
		'menu_icon'  =>  'dashicons-heart',
		'show_ui'  =>  true,
		'show_in_menu'  =>  true,
		'show_in_nav_menus'  =>  true,
		'show_in_admin_bar'  =>  true,
		'menu_position'  =>  4,
		'can_export'  =>  true,
		'has_archive'  =>  true,
		'exclude_from_search'  =>  false,
		'publicly_queryable'  =>  true,
		'capability_type'  =>  'post',
	);

	register_post_type('pair', $args);
}, 0, 20);
