<?php

namespace Deployer;

require 'recipe/common.php';

set('application', 'bedepo');

set('repository', 'https://gitlab+deploy-token-426684:qW5-bZCh5z3pyEzyYeDh@gitlab.com/Lukydemboy/bedepo.git');

set('ssh_multiplexing', true); // Speed up deployment

set('shared_files', [
    '.env',
    'web/.htaccess'
]);

set('shared_dirs', [
    'web/app/uploads'
]);

set('writable_dirs', []);

set('allow_anonymous_stats', false);

// Configuring the rsync exclusions.
// You'll want to exclude anything that you don't want on the production server.
add('rsync', [
    'exclude' => [
        '.gitlab-ci.yml',
        'deploy.php',
    ],
]);

host('bedepo-reptiles.com')
    ->stage('production')
    ->user('bedepo1q')
    ->port(4000)
    ->set('writable_mode', 'chmod')
    ->set('http_user', 'bedepo1q')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->set('deploy_path', '~/website');

/**
 * Tasks
 */
task('app:permissions', function () {
	run('find {{deploy_path}} -type d -exec chmod 0755 {} +');
	run('find {{deploy_path}} -type f -exec chmod 0644 {} +');
});

task('sage:vendors', function () {
    within('{{release_path}}/web/app/themes/sage', function () {
        run('composer install');
    });
});

task('assets:upload', function () {
    upload('web/app/themes/sage/dist', '{{release_path}}/web/app/themes/sage');
});

desc('Deploy your project');
task('deploy', [
    // 'assets:build',
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'sage:vendors',
    'assets:upload',
    'app:permissions',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// after('deploy:symlink', 'opcache:clear');
